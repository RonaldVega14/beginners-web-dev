<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "survey";

try{
    //Making connection with the db
    $db =  new PDO("mysql: host=$servername; dbname=$dbname", $username, $password);

    $db ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "<script>console.log('Connected successfully');</script>";

} catch (PDOException $e){
    echo "Connection failed: ". $e->getMessage();
}
?>