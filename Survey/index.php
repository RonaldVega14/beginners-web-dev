<?php

include_once 'connection.php';
$error="";
//Form data

//Verifying if is a POST connection
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //Checking if the form data is not empty, if not empty storing data in variables
    if (empty($_POST["name"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        Name is required
        </div>';
    }else {
        $name = $_POST["name"];
    }
    if (empty($_POST["gpa"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        GPA is required
        </div>';
    }else {
        $gpa = $_POST["gpa"];
    }
    if (empty($_POST["college"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        College is required
        </div>';
    }else {
        $college = $_POST["college"];
    }
    if (empty($_POST["class_year"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        Class Year is required
        </div>';
    }else {
        $class_year = $_POST["class_year"];
    }
    if (empty($_POST["high_school"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        High School is required
        </div>';
    }else {
        $high_school = $_POST["high_school"];
    }
    if (empty($_POST["email"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        Email is required
        </div>';
    }else {
        $email = $_POST["email"];
    }
    if (empty($_POST["funding_source"])) {
        $error .= '
        <div class="alert alert-danger" role="alert">
        Funding Source is required
         </div>';
    }else {
        $funding_source = $_POST["funding_source"];
    }
    if (empty($error)) {
        // Store data into db
        $stmt = $db->prepare("INSERT INTO inputs (gpa, name, email, college, class_year, high_school, funding_source) VALUES(:gpa, :name, :email, :college, :class_year, :high_school, :funding_source) ");
        //Binding variables
        $stmt->bindParam(':gpa', $gpa);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':college', $college);
        $stmt->bindParam(':class_year', $class_year);
        $stmt->bindParam(':high_school', $high_school);
        $stmt->bindParam(':funding_source', $funding_source);
        //execute statement
        $stmt->execute();

        $error = '
        <div class="alert alert-success" role="alert">
        Success, thank you for submitting!
      </div>';


    }else{
        //echo $error;
    }
    
}

?>

<?php include "templates/header.php" ?>
<section id="surveyForm">
<!-- Contenedor para la forma -->
    <div class="container">
        <div class="row mx-0">
            <div class = "col-12">
            <?php echo $error;?>
            <!-- Clase utilizada para la card en donde ira la form -->
                <div class="card tablem">
                    <div class="card-body">
                        <h5 class="card-title">College Funding Survey</h5>
                        <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="row mx-0 px-0">
                        
                            <!-- Form group para el nombre -->
                            <div class="form-group col-12 col-md-6">
                                <label for="name">Full name</label>
                                <input type="text" class="form-control" name = "name" id="name" aria-describedby="nameHelp" placeholder="Enter Full Name">
                            </div>
                            <!-- Form group para el email -->
                            <div class="form-group col-12 col-md-6">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" name = "email" id="email" aria-describedby="emailHelp" placeholder="Enter Email">
                            </div>
                            <!-- Form group para el gpa -->
                            <div class="form-group col-12 col-md-6">
                                <label for="gpa">GPA</label>
                                <input type="number" step=".01"  class="form-control" name = "gpa" id="gpa" aria-describedby="gpaHelp" placeholder="Enter GPA">
                            </div>
                            <!-- Form group para el Class Year -->
                            <div class="form-group col-12 col-md-6">
                                <label for="class_year">Class Year</label>
                                <input type="number"  min="1950" max="9999"  class="form-control" name = "class_year" id="class_year" aria-describedby="class_yearHelp" placeholder="Enter Class Year">
                            </div>
                            <!-- Form group para el high school -->
                            <div class="form-group col-lg-12 col-md-6">
                                <label for="high_school">High School</label>
                                <input type="text" class="form-control" name = "high_school" id="high_school" aria-describedby="high_schoolHelp" placeholder="Enter High School">
                            </div>
                            <!-- Form group para el college -->
                            <div class="form-group col-lg-12 col-md-6">
                                <label for="college">College</label>
                                <input type="text" class="form-control" name = "college" id="college" aria-describedby="collegeHelp" placeholder="Enter College">
                            </div>
                            <!-- Form group para el funding source -->
                            <div class="form-group col-lg-12 col-md-6">
                                <label for="funding_source">Funding Source</label>
                                <input type="text" class="form-control" name = "funding_source" id="funding_source" aria-describedby="funding_sourceHelp" placeholder="Enter Funding Source">
                            </div>
                            <!-- Form group para el boton de submit -->
                            <div class="form-group col-lg-12 col-md-6">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="/Survey/admin"> Survey Admin</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "templates/footer.php" ?>