<?php

$msg="";
// Display all of the inputs in a table
function inputTable(){
    include 'connection.php';
    $sql = "SELECT * FROM inputs ORDER By date DESC";
    $results = $db->query($sql);
    if($results->rowCount() > 0){
        while($row = $results->fetch(PDO::FETCH_ASSOC)) {
            echo '<tr>
            <td>'.$row["id"].'</td>
            <td>'.$row["gpa"].'</td>
            <td>'.$row["name"].'</td>
            <td>'.$row["email"].'</td>
            <td>'.$row["college"].'</td>
            <td>'.$row["class_year"].'</td>
            <td>'.$row["high_school"].'</td>
            <td>'.$row["funding_source"].'</td>
            <td>'.$row["date"].'</td>
            <td><a href="/Survey/admin?item=delete&id='.$row["id"].'" onClick="return confirm(\'Are you sure you want to delete this record?\')">Delete</a></td>
            </tr>';
        }
    }
}
//Display how many inputs were submitted
function inputTotal(){
    include 'connection.php';
    $sql = "SELECT * FROM inputs";
    $results = $db->query($sql);
    $inputNum = $results->rowCount();
    echo $inputNum;
}
//Display the average gpa of all inputs
function avgGpa(){
    include 'connection.php';
    $sql = "SELECT AVG(gpa) AS gpa FROM inputs";
    $results = $db->query($sql);
    $avgGpa = $results->fetch(PDO::FETCH_ASSOC);
    echo round($avgGpa['gpa'], 2);
}

//Display top colleges
function topColleges(){
    include 'connection.php';

    $sql = "SELECT id, college, max_count FROM 
    (SELECT id, college, count(college) as 
    max_count FROM inputs GROUP BY college) t 
    ORDER BY `t`.`max_count` DESC LIMIT 1 ";

    $results = $db->query($sql);
    $topColleges = $results->fetch(PDO::FETCH_ASSOC);
    echo $topColleges['college'];
}

//Display top funding sources
function topFundingSources(){
    include 'connection.php';

    $sql = "SELECT id, funding_source, max_count FROM
    (SELECT id, funding_source, count(funding_source) as 
    max_count FROM inputs GROUP BY funding_source) t 
    ORDER BY `t`.`max_count` DESC LIMIT 1 ";
    $results = $db->query($sql);
    $fundingSource = $results->fetch(PDO::FETCH_ASSOC);
    echo $fundingSource['funding_source'];
}

//Delete input from table using url for params
function deleteInputs(){
    include 'connection.php';
    
    if(empty($_GET['item']) || empty($_GET['id'])){
        return;
    }else{
        $item = $_GET['item'];
        $id = $_GET['id'];

        if ($item != 'delete') {
            return;
        }else{
            if (empty($id)) {
                return;
            } else {
                $sql = "DELETE FROM inputs WHERE id=:id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':id', $id);
                $stmt->execute();
                $msg ='
                    <div class="alert alert-success" role="alert">
                    Form Successfully deleted!
                    </div>
                    ';
            }
        }
    }
}
deleteInputs();
?>
<?php include "templates/header.php" ?>
<section id="surveyForm">
<!-- Contenedor para la forma -->
    <div class="container">
        <div class="row mx-0">
            <div class = "col-12 col-md-6">
            <!-- Clase utilizada para la card en donde ira la form -->
                <div class="card numc">
                    <h1><small>TOTAL INPUTS</small></h1>
                    <h1 class="infotext"> <?php inputTotal(); ?> </h1>
                </div>
            </div>
            <div class = "col-12 col-md-6">
            <!-- Clase utilizada para la card en donde ira la form -->
                <div class="card numc">
                    <h1><small>AVG GPA</small></h1>
                    <h1 class="infotext"> <?php avgGpa(); ?></h1>
                </div>
            </div>
            <div class ="col-12 ">
                <div class = "card tablem table-responsive-lg">
                <table class="table tablecard table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">GPA</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">College</th>
                            <th scope="col">Year</th>
                            <th scope="col">High School</th>
                            <th scope="col">Funding Source</th>
                            <th scope="col">Date</th>
                            <th scope="col">Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php inputTable();?>
                        </tbody>
                        </table>
                </div>
            </div>
            <div class = "col-12 col-md-6">
            <!-- Clase utilizada para la card en donde ira la form -->
                <div class="card numc">
                    <h1><small>Top College</small></h1>
                    <h3 class="infotext"> <?php topColleges(); ?></h3>
                </div>
            </div>
            <div class = "col-12 col-md-6">
            <!-- Clase utilizada para la card en donde ira la form -->
                <div class="card numc">
                    <h1><small>Top Funding Source</small></h1>
                    <h3 class="infotext"> <?php topFundingSources(); ?></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "templates/footer.php" ?>