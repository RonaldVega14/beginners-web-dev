## Create the inputs table
CREATE TABLE `survey`.`inputs` ( 
    `id` INT(10) NOT NULL AUTO_INCREMENT, 
    `gpa` FLOAT(4) NOT NULL , 
    `name` TEXT NOT NULL , 
    `date` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) , 
    `email` VARCHAR(200) NOT NULL , 
    `college` VARCHAR(250) NOT NULL , 
    `class_year` YEAR(4) NOT NULL , 
    `high_school` VARCHAR(200) NOT NULL , 
    `funding_source` VARCHAR(200) NOT NULL ) 
    ENGINE = MyISAM; 

## 