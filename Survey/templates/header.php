<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" >
    <link rel="stylesheet" href="assets/css/fontawesome.css" >
    <link rel="stylesheet" href="assets/css/style.css" >

    <title>College Funding Survey</title>
  </head>
  <body class="bg-light">
  <!-- Navbar section -->
<nav class="navbar navbar-light ">
<div class="container">
    <a class="navbar-brand" href="/Survey">
        <img src="assets/img/myLogo.png" width="50" height="50" class="d-inline-block align-center" alt="logo image">
        College Funding Survey
    </a>
  </div>
</nav>